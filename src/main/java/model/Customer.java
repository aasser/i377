package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "CUSTOMER")
public class Customer {

    @Id
    @SequenceGenerator(name = "my_seq", sequenceName = "seq1", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "my_seq")
    private Long id;

    @NotNull
    @Size(min = 2, max =  15)
    @Column(name = "first_name")
    private String firstName;

    @NotNull
    @Size(min = 2, max =  15)
    @Column(name = "last_name")
    private String lastName;

    @NotNull
    @Size(min = 2, max =  15)
    @Pattern(regexp = "^[A-Za-z0-9]*$")
    @Column(name = "code")
    private String code;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "customer_id", nullable = false)
    private List<Phone> phones = new ArrayList<>();

    @Column(name = "type")
    private String type;

    public Customer(Long id, String firstname, String lastName, String code, String type){
        this.id = id;
        this.firstName = firstname;
        this.lastName = lastName;
        this.code = code;
        this.type = type;
    }

    public Customer(String firstname){
        this.firstName = firstname;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", code='" + code + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
