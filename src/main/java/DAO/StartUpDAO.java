package DAO;

import util.DbUtil;
import util.PropertyLoader;
import util.Util;

import java.sql.DriverManager;
import java.sql.SQLException;

public class StartUpDAO {

    private static String DB_URL = new PropertyLoader().getProperty("javax.persistence.jdbc.url");

    public void createSchema(){

        String schema = Util.readFileFromClasspath("schema.sql");

        try {DbUtil.insertFromString(DriverManager.getConnection(DB_URL), schema);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

}
