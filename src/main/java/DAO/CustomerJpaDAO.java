package DAO;

import model.Customer;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class CustomerJpaDAO implements CustomerDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional
    public void insertCustomer(Customer customer) {
        if (customer.getId() == null){
            em.persist(customer);
        } else {
            em.merge(customer);
        }
    }

    @Override
    public Customer getCustomerById(Long id) {
        return em.createQuery("SELECT p FROM Customer p WHERE p.id = :id", Customer.class)
                .setParameter("id", id)
                .getSingleResult();

    }

    @Override
    public List<Customer> getCustomers() {

            return em.createQuery(
                    "SELECT DISTINCT p FROM Customer p left join fetch p.phones", Customer.class)
                    .getResultList();

    }

    @Override
    @Transactional
    public void deleteCustomerById(Long id) {

        em.createQuery("delete from Customer p where p.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    @Transactional
    public void delete() {

        em.createQuery("delete from Phone p")
                .executeUpdate();

        em.createQuery("delete from Customer p")
                .executeUpdate();
    }

    @Override
    public List<Customer> getList(String key) {
        return em.createQuery(
                "SELECT p FROM Customer p left join fetch p.phones " +
                        "WHERE lower(p.firstName) LIKE CONCAT('%', lower(:key), '%') " +
                        "OR lower(p.lastName) LIKE CONCAT('%', lower(:key), '%') " +
                        "OR lower(p.code) LIKE CONCAT('%', lower(:key), '%')", Customer.class)
                .setParameter("key", key)
                .getResultList();
    }




}
