package DAO;

import model.Customer;

import java.util.List;

public interface CustomerDAO {

    void insertCustomer(Customer customer);

    List<Customer> getCustomers();

    void deleteCustomerById(Long id);

    Customer getCustomerById(Long id);

    void delete();

    List<Customer> getList(String key);
}
