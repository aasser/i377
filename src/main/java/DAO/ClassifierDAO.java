package DAO;

import java.util.ArrayList;
import java.util.List;

public class ClassifierDAO {

    public List<String> getPhoneTypes() {
        List<String> phones = new ArrayList<>();

        phones.add("phone_type.fixed");
        phones.add("phone_type.mobile");

        return phones;
    }


    public List<String> getCustomerTypes() {
        List<String> customers = new ArrayList<>();

        customers.add("customer_type.private");
        customers.add("customer_type.corporate");

        return customers;

    }

}
