package controller;

import DAO.CustomerDAO;
import model.Customer;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@RestController
public class CustomerController {

    @Resource
    private CustomerDAO dao;

    @GetMapping("customers")
    public List<Customer> getCustomers(){
        return dao.getCustomers();
    }

    @GetMapping("customers/{id}")
    public Customer getOne(@PathVariable Long id){
        return dao.getCustomerById(id);
    }

    @GetMapping("customers/search")
    public List<Customer> getList(@RequestParam(defaultValue = "") String key){
        return dao.getList(key);
    }


    @PostMapping("customers")
    public void insertCustomer(@RequestBody @Valid Customer customer){
        dao.insertCustomer(customer);
    }


    @DeleteMapping("customers/{id}")
    public void deleteCustomer(@PathVariable Long id){
        dao.deleteCustomerById(id);
    }

    @DeleteMapping("customers")
    public void delete(){
        dao.delete();
    }

}
